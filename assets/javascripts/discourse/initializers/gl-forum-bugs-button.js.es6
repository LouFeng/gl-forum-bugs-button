export default {
  name: 'issue-report-button',
  initialize(container) {
    const siteSettings = container.lookup('site-settings:main');
    const TopicFooterButtonsComponent = container.lookupFactory('component:d-navigation');

    TopicFooterButtonsComponent.reopen({
      actions: {
        clickButton() {
          window.open(siteSettings.gl_bugs_button_url, '_blank');
        }
      }
    });
  }
}
