export default {
  actions: {
    clickButton() {
      window.open(this.siteSettings.gl_bugs_button_url, '_blank');
    }
  }
}
